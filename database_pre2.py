import cassandra.cluster
import csv
import re


def connection():
    import cassandra.cluster
    cluster = cassandra.cluster.Cluster(['localhost'])
    session = cluster.connect('caitiany')
    return session

def databaseCreate_Q2(session):
    query = """ 
	CREATE TABLE database_time ( 
        date varchar,
        time varchar,
        lon float,
        lat float,
        station varchar,
		tmpf float,
		dwpf float,
		relh float,
		drct float,
		sknt float,
		p01i float,
		alti float,
		mslp float,
		vsby float,
		gust float,
		skyc1 varchar,
		skyc2 varchar,
		skyc3 varchar,
		skyc4 varchar,
		skyl1 float,
		skyl2 float,
		skyl3 float,
		skyl4 float,
		wxcodes varchar,
		ice_accretion_1hr float,
		ice_accretion_3hr float,
		ice_accretion_6hr float,
		peak_wind_gust float,
		peak_wind_drct float,
		peak_wind_time varchar,
		feel float,
		metar varchar,
		PRIMARY KEY ((date),time,lon,lat,station)
	)"""
    session.execute(query)
    print("DATA BASE database_time created!")


def load_data(filename):
    with open(filename) as f:
        dateparser = re.compile("(?P<date>\d+-\d+-\d+) (?P<time>\d+:\d+)")
        for r in csv.DictReader(f):
            match_time = dateparser.match(r["valid"])
            if not r["valid"]:
                continue
            time = match_time.groupdict()
            for colonne in r:
                if r[colonne] == "M":
                    r[colonne]= "nan"
            
            data = {}
            data["date"] = time["date"]
            data["station"] = r["station"]
            data["time"] = time["time"]
            data["lon"] = float(r["lon"])
            data["lat"] = float(r["lat"])
            data["tmpf"] = float(r["tmpf"])
            data["dwpf"] = float(r["dwpf"])
            data["relh"] = float(r["relh"])
            data["drct"] = float(r["drct"])
            data["sknt"] = float(r["sknt"])
            data["p01i"] = float(r["p01i"])
            data["alti"] = float(r["alti"])
            data["mslp"] = float(r["mslp"])
            data["vsby"] = float(r["vsby"])
            data["gust"] = float(r["gust"])

            data["skyc1"] = r["skyc1"]
            data["skyc2"] = r["skyc2"]
            data["skyc3"] = r["skyc3"]
            data["skyc4"] = r["skyc4"]

            data["skyl1"] = float(r["skyl1"])
            data["skyl2"] = float(r["skyl2"])
            data["skyl3"] = float(r["skyl3"])
            data["skyl4"] = float(r["skyl4"])
            data["wxcodes"] = r["wxcodes"]

            data["ice_accretion_1hr"] = float(r["ice_accretion_1hr"])
            data["ice_accretion_3hr"] = float(r["ice_accretion_3hr"])
            data["ice_accretion_6hr"] = float(r["ice_accretion_6hr"])
            data["peak_wind_gust"] = float(r["peak_wind_gust"])
            data["peak_wind_drct"] = float(r["peak_wind_drct"])
            data["peak_wind_time"] = r["peak_wind_time"]

            data["feel"] = float(r["feel"])
            data["metar"] = r["metar"]

            yield data


#Create the query according to if each collonne's value is null or not
def createQuery(data):
    result = dict()
    for each in data:
        if data[each] != "nan" and  str(data[each]) != 'nan':
            result[each] = data[each]

    ligne_value = []
    for each in result:
        ligne_value.append(result[each])
    ligne_value = tuple(ligne_value)

    ligne = []
    for each in result:
        ligne.append(each)
    ligne = tuple(ligne)

    #connect the query together
    query = "INSERT INTO database_time("
    for eachc in ligne:
        query += str(eachc)+","
    query = "".join(list(query)[:-1]) + ") VALUES("
    longth = len(ligne)
    for i in range(longth):
        query += "%s,"
    query = "".join(list(query)[:-1]) + ");"

    return query, ligne_value

def insertion_sql_Q2(filename,session):
    target = load_data(filename)
    i = 1
    for data in target:
        i += 1
        k = 0
        if (i % 500 == 0):
            k += 1
            print(k,". 500 finished.....")
        query, ligne = createQuery(data)
        session.execute(query, ligne)


if __name__ == "__main__":
    session = connection()
    databaseCreate_Q2(session)
    insertion_sql_Q2("Projet-NF26/data.csv",session)
