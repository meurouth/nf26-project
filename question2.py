import numpy as np
import matplotlib.pyplot as plt
from functools import reduce
from database_pre2 import connection
import re
import folium


def createMap(data):
    mean_lat = 0
    mean_lon = 0
    count = 0
    for each in data.result():
        #print(each)
        mean_lat += each[2]
        mean_lon += each[3]
        count += 1
    if count == 0:
        print('No data available at this timestamp !')
        return
    mean_lat = mean_lat/count
    mean_lon = mean_lon/count

    m = folium.Map(location=[mean_lon,mean_lat], zoom_start=6)

    # Attributes names which will be displayed on the map
    attributes = ["alti", "drct", "dwpf", "feel", "gust", "ice_accretion_1hr", "ice_accretion_3hr", "ice_accretion_6hr",
                  "metar", "mslp", "p01i", "peak_wind_drct", "peak_wind_gust", "peak_wind_time", "relh", "sknt",
                  "skyc1", "skyc2", "skyc3", "skyc4", "skyl1", "skyl2", "skyl3", "skyl4", "tmpf", "vsby", "wxcodes"]
    for each in data.result():
        # Here we choose not to display the None values and the METAR ID
        ls = ["station:"+each[4]+"\n"]
        l = [attributes[i] + ":" + str(each[i + 5]) for i in range(len(attributes)) if each[i + 5] != None
             and attributes[i] != "metar"]
        l = ls + l
        string='\n'.join(l)
        folium.Marker([each[3],each[2]],
                      popup=string,
                      icon=folium.Icon(color='red')).add_to(m)
    m.save("Projet-NF26/map2.html")
    print("Generated successfully!")


if __name__ == "__main__":
    session = connection()
    # Timestamp user wants to search
    date = input("Please enter the date you want to search (format 'yyyy-MM-dd'):")
    time = input("Please enter the time you want to search (format 'hh:MM'):")
    #timestamp = '2017-12-02 00:30:00'
    data = session.execute_async("select * from caitiany.database_time where date = '%s' AND time = '%s'"%(date,time))
    createMap(data)
