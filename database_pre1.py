import cassandra.cluster
import csv
import re


def connection():
    import cassandra.cluster
    cluster = cassandra.cluster.Cluster(['localhost'])
    session = cluster.connect('caitiany')
    return session

def databaseCreate_Q1(session):
    query = """ 
	CREATE TABLE database_espace (
        station varchar,
        year int,
        season int,
        month int,
        day int,
        hour int,
        minute int,
        lon float,
        lat float,
		tmpf float,
		dwpf float,
		relh float,
		drct float,
		sknt float,
		p01i float,
		alti float,
		mslp float,
		vsby float,
		gust float,
		skyc1 varchar,
		skyc2 varchar,
		skyc3 varchar,
		skyc4 varchar,
		skyl1 float,
		skyl2 float,
		skyl3 float,
		skyl4 float,
		wxcodes varchar,
		ice_accretion_1hr float,
		ice_accretion_3hr float,
		ice_accretion_6hr float,
		peak_wind_gust float,
		peak_wind_drct float,
		peak_wind_time varchar,
		feel float,
		metar varchar,
		PRIMARY KEY ((station),year, season, month,day,hour,minute)
	)"""
    session.execute(query)
    print("DATA BASE database_espace created!")


def load_data(filename):
    dateparser = re.compile("(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+)")
    with open(filename) as f:
        for r in csv.DictReader(f):
            match_time = dateparser.match(r["valid"])
            if not match_time:
                continue
            time = match_time.groupdict()
            #add season
            if 3<=int(time["month"])<=5:
                r["season"]=0
            elif 6<=int(time["month"])<=8:
                r["season"]=1
            elif 9<=int(time["month"])<=11:
                r["season"]=2
            elif int(time["month"]) in (12,1,2):
                r["season"]=3
            else:
                continue

            for collonne in r:
                if r[collonne] == "M":
                    r[collonne]= "nan"
            
            data = {}
            data["station"] = r["station"]
            data["year"] = int(time["year"])
            data["season"] = int(r["season"])
            data["month"] = int(time["month"])
            data["day"] = int(time["day"])
            data["hour"] = int(time["hour"])
            data["minute"] = int(time["minute"])
            data["lon"] = float(r["lon"])
            data["lat"] = float(r["lat"])
            data["tmpf"] = float(r["tmpf"])
            data["dwpf"] = float(r["dwpf"])
            data["relh"] = float(r["relh"])
            data["drct"] = float(r["drct"])
            data["sknt"] = float(r["sknt"])
            data["p01i"] = float(r["p01i"])
            data["alti"] = float(r["alti"])
            data["mslp"] = float(r["mslp"])
            data["vsby"] = float(r["vsby"])
            data["gust"] = float(r["gust"])

            data["skyc1"] = r["skyc1"]
            data["skyc2"] = r["skyc2"]
            data["skyc3"] = r["skyc3"]
            data["skyc4"] = r["skyc4"]

            data["skyl1"] = float(r["skyl1"])
            data["skyl2"] = float(r["skyl2"])
            data["skyl3"] = float(r["skyl3"])
            data["skyl4"] = float(r["skyl4"])
            data["wxcodes"] = r["wxcodes"]

            data["ice_accretion_1hr"] = float(r["ice_accretion_1hr"])
            data["ice_accretion_3hr"] = float(r["ice_accretion_3hr"])
            data["ice_accretion_6hr"] = float(r["ice_accretion_6hr"])
            data["peak_wind_gust"] = float(r["peak_wind_gust"])
            data["peak_wind_drct"] = float(r["peak_wind_drct"])
            data["peak_wind_time"] = r["peak_wind_time"]

            data["feel"] = float(r["feel"])
            data["metar"] = r["metar"]

            yield data




#Create the query according to if each collonne's value is null or not
def createQuery(data):
    result = dict()
    for each in data:
        if data[each] != "nan" and  str(data[each]) != 'nan':
            result[each] = data[each]

    ligne_value = []
    for each in result:
        ligne_value.append(result[each])
    ligne_value = tuple(ligne_value)

    ligne = []
    for each in result:
        ligne.append(each)
    ligne = tuple(ligne)

    #connect the query together
    query = "INSERT INTO database_espace("
    for eachc in ligne:
        query += str(eachc)+","
    query = "".join(list(query)[:-1]) + ") VALUES("
    longth = len(ligne)
    for _ in range(longth):
        query += "%s,"
    query = "".join(list(query)[:-1]) + ");"

    return query, ligne_value



def insection_sql_Q1(filename,session):
    target = load_data(filename)
    i = 1
    for data in target:
        i += 1
        if (i % 500 == 0):
            print("500 finished.....")
        
        query, ligne = createQuery(data)

        session.execute(query, ligne)


if __name__ == "__main__":
    session = connection()
    databaseCreate_Q1(session)
    insection_sql_Q1("Projet-NF26/data.csv",session)