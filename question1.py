import numpy as np
import matplotlib.pyplot as plt
from functools import reduce
from database_pre1 import connection
import matplotlib.pyplot as plt


table_variable = ['station varchar',
        'year',
        'season',
        'month',
        'day',
        'hour',
        'minute',
        'lon',
        'lat',
		'tmpf',
		'dwpf',
		'relh',
		'drct',
		'sknt',
		'p01i',
		'alti',
		'mslp',
		'vsby',
		'gust',
		'skyc1',
		'skyc2',
		'skyc3',
		'skyc4',
		'skyl1',
		'skyl2',
		'skyl3',
		'skyl4',
		'wxcodes',
		'ice_accretion_1hr',
		'ice_accretion_3hr',
		'ice_accretion_6hr',
		'peak_wind_gust',
		'peak_wind_drct',
		'peak_wind_time',
		'feel',
		'metar']

def add (x,y):
    return x+y

def fmax(x,y):
    return max(x,y)

def fmin(x,y):
    return min(x,y)

#caculate mean, max, min reduce
#input [count,mean,max,min]
def reduceFonction (x,y):
    result = []
    fonctions = {0:'add',1:'add',2:'fmax',3:'fmin'}
    for i in range(4):
        result.append(reduce(eval(fonctions.get(i)),[x[i],y[i]]))
    return result

#input [valeur] -> [count,mean,max,min]
def mapFonction1 (x):
    return [1,x,x,x]

#input [count,mean,max,min] -> [mean,max,min]
def mapFonction2 (x):
    return [x[1]/x[0],x[2],x[3]]


#Map reduce fonction
def mapReduce_mmm(timeNB,targetNB,espace):
    results = dict()
    for row in session.execute("select * from caitiany.database_espace where station = '%s'"%espace ):
        if timeNB == 1:
            data_time = row[timeNB]
        elif timeNB == 2:
            data_time = (row[timeNB-1],row[timeNB])
        elif timeNB == 3:
            data_time = (row[timeNB-2],row[timeNB-1],row[timeNB])
        else:
            assert 1==2, "Doesn\'t exits!"
        data_target = row[targetNB]
        if str(data_time) == 'null' or data_target == None:
            continue
        if results.get(data_time) is None:
            results[data_time] = mapFonction1(data_target)
        else:
            mapresult = mapFonction1(data_target)
            results[data_time] = reduceFonction(mapresult,results[data_time])
    for eachTime in results:
        results[eachTime] = mapFonction2(results[eachTime])
    return results

#Zip the values in 3 list, one by one
def zipValues (values):
    result = [[],[],[]]
    for i in range(3):
        for each in values:
            result[i].append(each[i])
    return result

#Fonction 1: the history courbe graph
def drawCourbe_history(session,time,target,timeNB,targetNB,espace):
    #data = session.execute_async("select * from caitiany.database_espace where station = '%s'"%espace )
    results = mapReduce_mmm(timeNB,targetNB,espace)
    keys = list(results.keys())
    values = list(results.values())
    zipped_result = zipValues(values)
    if isinstance(keys[0],tuple):
        for each in keys:
            index_each = keys.index(each)
            each = list(each)
            if timeNB == 2:
                each[0] = str(each[0])
                if each[1] == 0:
                    each[1] = "Spring"
                elif each[1] == 1:
                    each[1] = "Summer"
                elif each[1] == 2:
                    each[1] = "Autumn"
                elif each[1] == 3:
                    each[1] = "Winter"
                keys[index_each] = ".".join(each)
            elif timeNB == 3:
                each[0] = str(each[0])
                each[2] = str(each[2])
                keys[index_each] = each[0] +'.'+ each[2]
    fig, ax = plt.subplots(1, 1)
    plt.plot(keys,zipped_result[0],'x-',label="mean")
    plt.plot(keys,zipped_result[1],'+-',label="max")
    plt.plot(keys,zipped_result[2],'b--',label="min")
    plt.xticks(keys, keys, rotation=45, fontsize=5)
    plt.ylabel(target)
    plt.grid(True)
    if timeNB == 3:
        for label in ax.get_xticklabels():
            label.set_visible(False)
        for label in ax.get_xticklabels()[::6]:
            label.set_visible(True)
    plt.legend(bbox_to_anchor=(1.0, 1), loc=1, borderaxespad=0.)
    plt.savefig("Projet-NF26/question1.png")
    print ("Generate successfully")


#Check which number of the indicateur
def checkNBvariable (x):
    i=0
    for each in table_variable:
        if x == each:
            return i
        i += 1
    print ('Doesn\'t exist!!')




#Caculate the mean of the values of each season
def caculateMean_Season(result):
    i = 0
    total = 0
    for each in result:
        total += each
        i += 1
    return total/i , i

#Draw the courbe of the fonction2
def drawCourbe_season(session,season,target,targetNB,espace):
    #data = session.execute_async("select * from caitiany.database_espace where station = '%s'"%espace )
    #We do the same map reduce as fonction 1 by fixing the time as season
    results = mapReduce_mmm(2,targetNB,espace)
    results = seprateSeason(results,season)
    keys = list(results.keys())
    values = list(results.values())
    zipped_result = zipValues(values)
    for each in keys:
        index_each = keys.index(each)
        each = list(each)
        each[0] = str(each[0])
        if each[1] == 0:
            each[1] = "Spring"
        elif each[1] == 1:
            each[1] = "Summer"
        elif each[1] == 2:
            each[1] = "Autumn"
        elif each[1] == 3:
            each[1] = "Winter"
        keys[index_each] = ".".join(each)

    fig, ax = plt.subplots(1, 1)
    mean_season_mean, longth = caculateMean_Season(zipped_result[0])
    plt.plot(keys,zipped_result[0],'x-',label="mean")
    plt.plot(keys,[mean_season_mean for i in range(longth)],'--')

    mean_season_mean, longth = caculateMean_Season(zipped_result[1])
    plt.plot(keys,zipped_result[1],'+-',label="max")
    plt.plot(keys,[mean_season_mean for i in range(longth)],'--')

    mean_season_mean, longth = caculateMean_Season(zipped_result[2])
    plt.plot(keys,zipped_result[2],'b--',label="min")
    plt.plot(keys,[mean_season_mean for i in range(longth)],'--')

    plt.xticks(keys, keys, rotation=45, fontsize=5)
    plt.ylabel(target)
    plt.grid(True)
    plt.legend(bbox_to_anchor=(1.0, 1), loc=1, borderaxespad=0.)
    plt.savefig("Projet-NF26/question1_season.png")
    print ("Generate successfully")


#Choose the data of the season we want
def seprateSeason (results,season):
    output = dict()
    if season == "Spring":
        season = 0
    elif season == "Summer":
        season = 1
    elif season == "Autumn":
        season = 2
    elif season == "Winter":
        season = 3
    for each in results:
        if season in each:
            output[each] = results[each]
    return output



if __name__ == "__main__":
    session = connection()
    choice = int(input("Which kind of service do you want?\n1.Station history\n2.Check history by seasons\nYour choice: "))

    #if choice == 1, we will use the fonction 1
    if choice == 1:
        espace = input("Please enter which station you want to search [LEBZ,LETO,etc]:  ")
        time = input("By which kind of time [year,season,month]:  ")
        target = input("Which indicator do you want to check [tmpf,dwpf,etc]:  ")
        timeNB = checkNBvariable(time)
        targetNB = checkNBvariable(target)
        drawCourbe_history(session,time,target,timeNB,targetNB,espace)
    else:
        #Fonction 2, the check according to the seasons
        espace = input("Please enter what station you want to search [LEBZ,LETO,etc]:  ")
        target = input("Which indicator do you want to check [tmpf,dwpf,etc]:  ")
        targetNB = checkNBvariable(target)
        season = input("Please enter which season you want to search [Spring,Summer,Autumn,Winter]:  ")
        drawCourbe_season(session,season,target,targetNB,espace)
